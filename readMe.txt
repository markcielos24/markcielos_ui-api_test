How to Run:
	1. Open project in Eclipse then go to Project tab > Clean
	2. Open TestRunner.java
		RUNNER
			//src/test/java/StepsDefinitions/TestRunner.java	
	3. Ensure desired "tags" are selected
	4. Go to Run tab and click Run button (CTRL+F11)
	5. After execution result will be found in target folder
	6. Screenshots will full page can be found in //src/test/resource/screenshots
		
FEATURES
	//scr/test/resources/Features/
	UI Framework
		Enquire_NewHomeLoan.feature
	API Automation
		Weatherbit_Get_CurrentWeather.feature
		Weatherbit_Get_DailyForecast.feature (note: Daily Forecast API was used as Hourly Forecast API does not accept API key from free users)
